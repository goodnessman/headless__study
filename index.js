import Koa from 'koa';
import Router from '@koa/router';
import p from 'phin';

const app = new Koa();
const router = new Router();
const HTML_TYPE = 'html';
const STATUS_OK = 200;
const STATUS_ERROR = 404;
const ERROR_MESSAGE = 'Error!';

router.get('/prod1', async ctx => {
    try {
        const response = await p('https://ethanent.me');
        ctx.status = STATUS_OK;
        ctx.type = HTML_TYPE;
        ctx.body = response.body;
    } catch(e) {
        ctx.status = STATUS_ERROR;
        ctx.body = ERROR_MESSAGE;
    }
});

router.get('/prod2', async ctx => {
    try {
        const response = await p('https://astoundcommerce.com');
        ctx.status = STATUS_OK;
        ctx.type = HTML_TYPE;
        ctx.body = response.body;
    } catch(e) {
        ctx.status = STATUS_ERROR;
        ctx.body = ERROR_MESSAGE;
    }
});

app.use(router.routes())
    .use(router.allowedMethods());

app.listen(3000);